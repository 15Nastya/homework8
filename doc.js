'use strict'
const person = {
    age: 45,
    height: 180,
    weight: 70,
    firstName: 'Антон',
    LastName: 'Иванов',
    body: {
        body: {
            body: {
                value: 1,
            }
        }
    }
}

// Объявляем пустой массив, в который будем складывать результаты
let result1 = ["{"]; // Инициализируем массив с открывающейся фигурной скобкой
let counter = 1; // Счётчик пробелов


// В цикле идём по "ключ: значение" и добавляем в массив
function recursion(person1, counter1) {
  for (const [key, value] of Object.entries(person1)) {
    if (typeof value !== "object") {
      result1.push("  ".repeat(counter1) + `${key}: ${value}`);
    } else {
      result1.push("  ".repeat(counter1) + `${key}: {`);

      recursion(value, counter1 + 1);

      result1.push("  ".repeat(counter1) + `}`);
    }
  }
}

recursion(person, counter);

result1.push("}"); // Заканчиваем закрывающейся фигурной скобкой

// Выводим результат

for (let i = 0; i < result1.length; i++) {
  console.log(result1[i]);
}





